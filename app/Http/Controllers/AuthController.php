<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        if (!auth()->attempt($request->only('email', 'password'))) {
            return $this->xSendError('Unauthorized.', ['error' => 'Unauthorized']);
        }

        $token = md5(time()); // Dummy

        return $this->xSendResponse($token, 'User login successfully.');
    }
}