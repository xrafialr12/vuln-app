<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function upload(Request $request)
    {
        // Validation by MIME JPG, PNG, GIF
        $this->validate($request, [
            'file' => 'required|max:2048',
        ]);
        
        // Upload File
        $file = $request->file('file');
        $name = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $file->move($destinationPath, $name);

        return back()
            ->with('success','You have successfully upload image.')
            ->with('image',$name);
    }
}
