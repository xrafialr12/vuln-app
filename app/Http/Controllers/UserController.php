<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return $this->xSendResponse($users, 'Users retrieved successfully.');
    }

    public function show($id)
    {
        $user = User::find($id);

        if (!$user) {
            return $this->xSendError('User not found.');
        }

        return $this->xSendResponse($user, 'User retrieved successfully.');
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);

        if (!$user) {
            return $this->xSendError('User not found.');
        }

        $user->name = $request->name ?? $user->name;
        $user->email = $request->email ?? $user->email;
        $user->save();

        return $this->xSendResponse($user, 'User updated successfully.');
    }
}
